%{
#include "y.tab.h"
void yyerror (char *error);
%}

%%
"db"					return DB;
"find"					return FIND;
"insert"				return INSERT;
"delete"				return DELETE;
"update"				return UPDATE;
"$parent"				return PARENT;
"$set"					return SET;
"$or"					return OR;
"$eq"					return EQ;
"$ne"					return NE;
"$gt"					return GT;
"$ge"					return GE;
"$lt"					return LT;
"$le"					return LE;
\(						return BRACE_CALL_BEGIN;
\)						return BRACE_CALL_CLOSE;
\[						return BRACE_LIST_BEGIN;
\]						return BRACE_LIST_CLOSE;
\{						return BRACE_DICT_BEGIN;
\}						return BRACE_DICT_CLOSE;
:						return COLON;
,						return COMMA;
(\"|\')					return QUOTE;
\.						return DOT;
(True|true|yes)			return TRUE;
(False|false|no)		return FALSE;
\".+?\"					{yylval.string = strdup(yytext); return STRING;}
[a-zA-Z][a-zA-Z_0-9]*	{yylval.string = strdup(yytext); return KEY;}
-?([0-9]+)?\.[0-9]+		{yylval.fnum = atof(yytext); return FLOAT_NUMBER;}
-?[0-9]+				{yylval.num = atoi(yytext); return INT_NUMBER;}
[ \t\n]					;
.						{yyerror("unexpected character");}

%%
