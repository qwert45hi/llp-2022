#ifndef NQ_2_MAIN_H
#define NQ_2_MAIN_H

#include <ctype.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

enum field_type {
    TYPE_INTEGER,
    TYPE_FLOAT,
    TYPE_BOOLEAN,
    TYPE_STRING,
};

enum operation_type {
    OP_EQ,
    OP_NE,
    OP_GT,
    OP_GE,
    OP_LT,
    OP_LE,
};

struct field_data {
    char* name;
    enum field_type type;
    uint64_t int_value;
    double real_value;
};

struct comparator {
    struct comparator* next;
    enum operation_type operation;
    struct field_data field_data;
};

struct filter {
    struct filter* next;
    struct comparator* comp_list;
};

struct value {
    struct value* next;
    struct field_data field_data;
};

struct query_tree {
    char* command;
    struct filter* filters;
    struct value* first_value;
};

#endif
