# pip install matplotlib
from collections.abc import Iterator
from pathlib import Path

import matplotlib.pyplot as plt


def iterate_points(filepath: Path) -> Iterator[tuple[float, float]]:
    with filepath.open() as f:
        for line in f.readlines():
            yield tuple(map(float, line.split(",")))


def extract_axis(array: list[tuple[float, float]], axis: int) -> list[float]:
    return [point[axis] for point in array]


datapoints = [point for point in iterate_points(Path("results.csv"))]
x_points = extract_axis(datapoints, 0)


def plot_one(column: int, name: str, filename: str) -> None:
    plt.clf()

    y_points = extract_axis(datapoints, column)

    plt.plot(x_points, y_points)
    plt.ylabel(name)
    plt.ylim([0, max(y_points) * 1.2])

    plt.xlabel("number of elements")
    plt.xlim([min(x_points), max(x_points)])
    plt.savefig(f"fig/{column}-{filename}.png")


GRAPHS: list[tuple[str, str]] = [
    ("time, ms", "creating"),
    ("time, ms", "reading"),
    ("time, ms", "listing"),
    ("file size, KiB", "size"),
    ("memory, KiB", "memory"),
]

for column, (name, filename) in enumerate(GRAPHS, start=1):
    plot_one(column, name, filename)
