#include "add.h"

int add_element(FILE *file, struct database_header *header, int argc, char **argv) {
    // name, size, created, updated, owner, group, content
    struct frame *frame = malloc(sizeof(struct frame));
    frame->data = malloc(sizeof(union data_type) * header->field_count);
    frame->header = malloc(sizeof(struct frame_header));
    frame->header->is_string = 0;
    frame->header->links.node_id = header->node_count;

    // printf("%d\n", argc);

    uint64_t temp_offset;
    for (uint64_t i = 0; i < header->field_count; i++) {
        if (i == 1) {  // integer
            frame->data[i].integer_data = atoi(argv[i + 1]);
        } else if (i == 2 || i == 3) {
            frame->data[i].float_data = atof(argv[i + 1]);
        } else {
            string_save(file, header, argv[i + 1], strlen(argv[i + 1]), &temp_offset,
                        0);  // TODO pass real
            frame->data[i].string_offset = temp_offset;
            printf("%lu: %lu (%lu)\n", i, temp_offset, header->frame_count);
        }
    }

    struct node *node = malloc(sizeof(struct node));
    node->parent_id = atoi(argv[0]);
    frame_append(file, header, frame, &node->frame_offset);
    printf("node: %lu %lu\n", node_offset(header, frame->header->links.node_id),
           node->frame_offset);
    node_append(file, header, node);

    frame_free(frame);
    free(frame);
    free(node);

    return 0;
}
