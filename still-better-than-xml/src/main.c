#include <inttypes.h>
#include <malloc.h>
#include <stdbool.h>
#include <stdio.h>
#include <string.h>

#ifdef POSIX
#include <unistd.h>
#endif

#include "backend/frames.h"
#include "backend/meta.h"
#include "backend/strings.h"
#include "commands/add.h"
#include "crud/nodes.h"
#include "crud/utils.h"
#include "structures.h"

bool is_file_empty(FILE *file) {
    long prev = ftell(file);
    fseek(file, 0, SEEK_END);
    bool is_empty = ftell(file) == 0;
    fseek(file, prev, SEEK_SET);
    return is_empty;
}

int test_file(FILE *file) {
    // TODO init fields
    uint64_t field_count = 3;

    uint64_t base_offset = field_count * sizeof(struct field) + sizeof(struct database_header);
    uint64_t frame_bytes = field_count * sizeof(union data_type) / sizeof(char);

    struct database_header database_header = {
        .field_count = field_count,
        .node_count = 0,
        .node_offset = base_offset,
        .frame_bytes = frame_bytes,
        .frame_size = frame_bytes + sizeof(struct frame_header),
        .frame_count = 0,
        .frame_offset = base_offset,
    };

    // printf("%lu %lu\n", &database_header.frame_bytes, &database_header.frame_size);

    header_save(file, &database_header);

    /* frames experiments */
    union data_type data[3] = {
        {.integer_data = -1},
        {.float_data = 1.43},
        {.boolean_data = true},
    };
    struct frame_header frame_header = {
        .is_string = false,
        .links =
            {
                .node_id = database_header.node_count,
            },
    };
    struct frame frame = {
        .header = &frame_header,
        .data = data,
    };

    /* node experiments */
    struct node *node = malloc(sizeof(struct node));
    node->parent_id = -1;
    // printf("%lu\n", node->frame_offset);
    node_seek(file, &database_header, database_header.node_count);
    fwrite(node, sizeof(struct node), 1, file);
    database_header.frame_offset += sizeof(struct node);
    database_header.node_count++;
    /* end: node experiments */

    // TODO adding links from user data
    // TODO removing links
    // TODO recursion for removing

    frame_append(file, &database_header, &frame, &(node->frame_offset));

    struct frame new_frame;
    frame_load(file, &database_header, &new_frame, node->frame_offset);
    // printf("%lu\n", new_frame.header->links.node_id);
    frame_free(&new_frame);
    /* end: frames experiments */

    /* string experiments */

    struct frame_header string_header = {
        .is_string = true,
        .links =
            {
                .prev_offset = 0,
                .next_offset = 0,
            },
    };
    char *line = "hey";
    struct string string = {
        .header = &string_header,
        .real_length = 3,
        .data = line,
    };

    database_seek_frames_end(file, &database_header);
    string_write(file, &database_header, &string);
    database_header.frame_count++;

    /* end: string experiments */

    /* long string write */

    char *long_string = "hello world, this is a long sentence or something";
    uint64_t len = 49;
    uint64_t start_offset;  // for reading later
    string_save(file, &database_header, long_string, len, &start_offset, 0);

    /* end: long string write */

    /* frame movement */
    uint64_t source = start_offset + database_header.frame_size;
    move_frame_to_end(file, &database_header, source);
    /* end: frame movement */

    /* long string read */

    char *result;
    string_load(file, &database_header, &result, start_offset);
    printf("result1: %s\n", result);
    database_header.frame_count++;

    /* end: long string read */

    /* frame movement back */
    uint64_t target = start_offset + database_header.frame_size;
    move_frame_from_end(file, &database_header, target);

    database_header.frame_count--;
    /*
#ifdef _WIN32
    _chsize(file, source);
#else
    ftruncate(file, source);
#endif
    */

    string_load(file, &database_header, &result, start_offset);
    printf("result2: %s\n", result);
    /* end: frame movement back */

    /* node creation */
    // allocate node
    node = malloc(sizeof(struct node));
    node->parent_id = 0;

    // write related frame(s?)
    frame.header->links.node_id = database_header.node_count;
    frame_append(file, &database_header, &frame, &(node->frame_offset));
    printf("node: %lu %lu (%lu)\n", node->frame_offset, node->parent_id,
           database_header.frame_offset);

    // move frame_offset if needed
    node_append(file, &database_header, node);

    // read the node back
    struct node *new_node = malloc(sizeof(struct node));
    node_load(file, &database_header, new_node, database_header.node_count - 1);
    printf("node: %lu %lu (%lu)\n", node->frame_offset, node->parent_id,
           database_header.frame_offset);

    /* end: node experiments */

    return 0;
}

int init_file(FILE *file) {
    // name, size, created, updated, owner, group, content
    uint64_t field_count = 7;

    uint64_t base_offset = field_count * sizeof(struct field) + sizeof(struct database_header);
    uint64_t frame_bytes = field_count * sizeof(union data_type) / sizeof(char);

    struct database_header database_header = {
        .field_count = field_count,
        .node_count = 0,
        .node_offset = base_offset,
        .frame_bytes = frame_bytes,
        .frame_size = frame_bytes + sizeof(struct frame_header),
        .frame_count = 0,
        .frame_offset = base_offset + sizeof(struct node) * 2048,
    };

    // printf("%lu %lu\n", &database_header.frame_bytes, &database_header.frame_size);

    header_save(file, &database_header);

    return 0;
}

int main(int argc, char **argv) {
    // fclose(fopen("tmp.nq", "w"));  clear the file

    fclose(fopen("tmp.nq", "a"));
    FILE *file = fopen("tmp.nq", "r+b");

    if (is_file_empty(file)) {
        printf("initializing file...\n");
        init_file(file);
    }
    // test_file(file);

    struct database_header *header = malloc(sizeof(struct database_header));
    header_load(file, header);

    printf("%s\n", argv[1]);
    if (!strcmp(argv[1], "add")) {
        add_element(file, header, argc - 2, argv + 2);
    }

    fclose(file);

    return 0;
}
