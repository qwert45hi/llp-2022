#ifndef NQ_1_FRAMES_H
#define NQ_1_FRAMES_H

#include <malloc.h>
#include <stdio.h>

#include "../structures.h"
#include "meta.h"

int frame_write(FILE *file, struct database_header *header, struct frame *frame);
void frame_init(struct database_header *header, struct frame *frame);
void frame_free(struct frame *frame);
int frame_header_read(FILE *file, struct database_header *header,
                      struct frame_header *frame_header);
int frame_data_read(FILE *file, struct database_header *header, struct frame *frame);
int frame_read(FILE *file, struct database_header *header, struct frame *frame);

int frame_seek(FILE *file, struct database_header *header, uint64_t offset);

int frame_load(FILE *file, struct database_header *header, struct frame *frame, uint64_t offset);

int frame_append(FILE *file, struct database_header *header, struct frame *frame, uint64_t *offset);

#endif
