#ifndef NQ_1_META_H
#define NQ_1_META_H

#include <stdio.h>

#include "../structures.h"

int header_write(FILE *file, struct database_header *header);
int header_save(FILE *file, struct database_header *header);

int header_read(FILE *file, struct database_header *header);
int header_load(FILE *file, struct database_header *header);

uint64_t database_frames_end_offset(struct database_header *header);
int database_seek_frames_end(FILE *file, struct database_header *header);

#endif
