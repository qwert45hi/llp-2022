#ifndef NQ_1_STRINGS_H
#define NQ_1_STRINGS_H

#include <malloc.h>
#include <stdio.h>
#include <string.h>

#include "../structures.h"
#include "frames.h"
#include "meta.h"

int string_write(FILE *file, struct database_header *header, struct string *string);
void string_init(struct database_header *header, struct string *string);
void string_free(struct string *string);
int string_data_read(FILE *file, struct database_header *header, struct string *string);
int string_read(FILE *file, struct database_header *header, struct string *string);

int string_save(FILE *file, struct database_header *header, char *source, uint64_t len,
                uint64_t *start_offset, uint64_t frame_offset);
int string_load(FILE *file, struct database_header *header, char **target, uint64_t offset);

#endif
