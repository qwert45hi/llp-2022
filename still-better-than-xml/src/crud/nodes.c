#include "nodes.h"

uint64_t node_offset(struct database_header *header, uint64_t node_id) {
    return header->node_offset + sizeof(struct node) * node_id;
}

int node_seek(FILE *file, struct database_header *header, uint64_t node_id) {
    fseek(file, node_offset(header, node_id), SEEK_SET);
    return 0;
}

int node_save(FILE *file, struct database_header *header, struct node *node, uint64_t node_id) {
    node_seek(file, header, node_id);
    fwrite(node, sizeof(struct node), 1, file);
    return 0;
}

int node_append(FILE *file, struct database_header *header, struct node *node) {
    if (node_offset(header, header->node_count + 1) > header->frame_offset) {
        printf("hey %lu\n", header->frame_offset);
        move_frame_to_end(file, header, header->frame_offset);
        header->frame_offset += header->frame_size;
        printf("hey %lu\n", header->frame_offset);
    }
    node_save(file, header, node, header->node_count);
    header->node_count++;
    header_save(file, header);
    return 0;
}

int node_read(FILE *file, struct database_header *header, struct node *node) {
    fread(node, sizeof(struct node), 1, file);
    return 0;
}

int node_load(FILE *file, struct database_header *header, struct node *node, uint64_t node_id) {
    node_seek(file, header, node_id);
    node_read(file, header, node);
    return 0;
}
