#ifndef NQ_1_UTILS_H
#define NQ_1_UTILS_H

#include <malloc.h>
#include <stdio.h>

#include "../backend/frames.h"
#include "../backend/meta.h"
#include "../backend/strings.h"
#include "../structures.h"
#include "nodes.h"

int move_frame(FILE *file, struct database_header *header, uint64_t source, uint64_t target);
int move_frame_to_end(FILE *file, struct database_header *header, uint64_t source);
int move_frame_from_end(FILE *file, struct database_header *header, uint64_t target);

int write_zeroes(FILE *file, uint64_t offset, uint64_t count);

#endif
