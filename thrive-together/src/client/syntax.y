%{
#include "main.h"

// yacc-related magic
int main(void) { return yyparse(); }
void yyerror(char* error) { fprintf(stderr, "%s\n", error); }
int yywrap(void) { return 1; }

// static structures
static struct query_tree tree;

static struct comparator* comparator;
static struct comparator* or_comparator = NULL;

// printing the resulting tree
void print_tree(char *file) {
    printf("file: %s\n", file);
    printf("command: %s\n", tree.command);
    printf("filters:\n");

    while (tree.filters) {
        while (tree.filters->comp_list) {
            char* field = tree.filters->comp_list->field_data.name;
            uint64_t value = tree.filters->comp_list->field_data.int_value;
            float field_dataalue = tree.filters->comp_list->field_data.real_value;

            printf("  - field: %s\n", field);
            printf("    comparator: %d\n", tree.filters->comp_list->operation);

            switch (tree.filters->comp_list->field_data.type) {
                case TYPE_INTEGER:
                    printf("    type: integer\n");
                    printf("    value: %d\n", value);
                    break;
                case TYPE_FLOAT:
                    printf("    type: float\n");
                    printf("    value: %f\n", field_dataalue);
                    break;
                case TYPE_BOOLEAN:
                    printf("    type: boolean\n");
                    printf("    value: %s\n", value ? "true" : "false");
                    break;
                case TYPE_STRING:
                    printf("    type: string\n");
                    printf("    value: %s\n", value);
                    break;
            }
            tree.filters->comp_list = tree.filters->comp_list->next;
        }
        tree.filters = tree.filters->next;
    }
}

void new_filter() {
    struct filter* filter = malloc(sizeof(struct filter));
    struct comparator* tmp = malloc(sizeof(struct comparator));
    filter->next = tree.filters;

    if (or_comparator) {
        tmp->next = malloc(sizeof(struct comparator));
        tmp->next->operation = or_comparator->operation;
        tmp->next->field_data = or_comparator->field_data;
    }
    tmp->operation = comparator->operation;
    tmp->field_data = comparator->field_data;

    if (tree.filters) {
        tree.filters->comp_list = tmp;
    } else {
        filter->comp_list = tmp;
        tree.filters = filter;
        filter = malloc(sizeof(struct filter));
        filter->next = tree.filters;
    }

    comparator = comparator->next;
    tree.filters = filter;
}

static enum field_type current_field_type;
struct field_data construct_field_data(char* field, uint64_t value, double float_value) {
    struct field_data field_data = {.name = field, .type = current_field_type};
    field_data.real_value = float_value;
    field_data.int_value = value;
    return field_data;
}

void set_current_operation(enum operation_type operation) {
    comparator = malloc(sizeof(struct comparator));
    comparator->next = comparator;
    comparator->operation = operation;
}

void append_value(struct field_data field_data) {
    struct value* vs = malloc(sizeof(struct value));
    vs->field_data = field_data;
    vs->next = tree.first_value;
    tree.first_value = vs;
}

%}

%union {uint64_t num; char *string; double fnum;}
%token DB
%token <string> FIND INSERT DELETE UPDATE
%token <string> PARENT KEY STRING
%token SET OR EQ NE GT GE LT LE
%token BRACE_CALL_BEGIN BRACE_CALL_CLOSE BRACE_DICT_BEGIN BRACE_DICT_CLOSE BRACE_LIST_BEGIN BRACE_LIST_CLOSE
%token COLON COMMA QUOTE DOT
%token <num> FALSE TRUE INT_NUMBER
%token <fnum> FLOAT_NUMBER
%type <num> bool value operation comp

%%

syntax:
    DB DOT KEY DOT command {print_tree($3);};

command:
    FIND BRACE_CALL_BEGIN BRACE_DICT_BEGIN filters BRACE_DICT_CLOSE BRACE_CALL_CLOSE {tree.command = "find";} |
    DELETE BRACE_CALL_BEGIN BRACE_DICT_BEGIN filters BRACE_DICT_CLOSE BRACE_CALL_CLOSE {tree.command = "delete";} |
    INSERT BRACE_CALL_BEGIN values BRACE_CALL_CLOSE {tree.command = "insert";} |
    UPDATE BRACE_CALL_BEGIN BRACE_DICT_BEGIN filters COMMA SET COLON values BRACE_DICT_CLOSE BRACE_CALL_CLOSE {tree.command = "update";};

filters:
    filter {new_filter();} |
    filter COMMA filters {new_filter();};

filter:
    simple_filter |
    OR BRACE_LIST_BEGIN simple_filter COMMA simple_filter BRACE_LIST_CLOSE {
        or_comparator = comparator->next;
        comparator->next = comparator->next->next;
    };

simple_filter:
    KEY COLON operation {comparator->field_data = construct_field_data($1, $3, $3);}

operation:
    BRACE_DICT_BEGIN comp COLON value BRACE_DICT_CLOSE {set_current_operation($2); $$ = $4;};

values:
    BRACE_DICT_BEGIN set_values BRACE_DICT_CLOSE;

set_values:
    set_value |
    set_value COMMA set_values

set_value:
    KEY COLON value {append_value(construct_field_data($1, $3, $3));};

value:
    INT_NUMBER {current_field_type = TYPE_INTEGER; $$ = $1;} |
    FLOAT_NUMBER {current_field_type = TYPE_FLOAT; $$ = $1;} |
    bool {current_field_type = TYPE_BOOLEAN; $$ = $1;} |
    STRING {current_field_type = TYPE_STRING; $$ = $1;};

bool:
    TRUE {$$ = 1;} |
    FALSE {$$ = 0;};

comp:
    EQ {$$ = 0;} |
    NE {$$ = 1;} |
    GT {$$ = 2;} |
    GE {$$ = 3;} |
    LT {$$ = 4;} |
    LE {$$ = 5;};

%%
