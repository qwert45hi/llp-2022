#include "strings.h"

int string_write(FILE *file, struct database_header *header, struct string *string) {
    fwrite(string->header, sizeof(struct frame_header), 1, file);
    fwrite(string->data, sizeof(char), string->real_length, file);
    for (uint64_t i = string->real_length; i < header->frame_bytes; i++) fputc(0, file);
    return 0;
}

void string_init(struct database_header *header, struct string *string) {
    string->data = malloc(sizeof(char) * (header->frame_bytes + 1));
    string->header = malloc(sizeof(struct frame_header));
}

void string_free(struct string *string) {
    free(string->data);
    free(string->header);
}

int string_data_read(FILE *file, struct database_header *header, struct string *string) {
    fread(string->data, sizeof(char), header->frame_bytes, file);
    string->data[header->frame_bytes] = 0;
    string->real_length = strlen(string->data);
    return 0;
}

int string_read(FILE *file, struct database_header *header, struct string *string) {
    frame_header_read(file, header, string->header);
    string_data_read(file, header, string);
    return 0;
}

int string_save(FILE *file, struct database_header *header, char *source, uint64_t len,
                uint64_t *start_offset, uint64_t frame_offset) {
    *start_offset = database_frames_end_offset(header);
    uint64_t offset = *start_offset;
    // printf("offset: %lu\n", offset);

    if (len == 0) {
        struct string *string = malloc(sizeof(struct string));
        string_init(header, string);

        string->real_length = 0;
        string->header->is_string = 1;
        string->header->links.prev_offset = frame_offset;
        string->header->links.next_offset = 0;

        frame_seek(file, header, offset);
        string_write(file, header, string);
        header->frame_count += 1;
        header_save(file, header);

        string_free(string);
        return 0;
    }

    uint64_t countm1 = ((len - 1) / header->frame_bytes);
    // printf("%lu\n", countm1);

    struct string *strings = malloc(sizeof(struct string) * (countm1 + 1));

    uint64_t j = 0;
    for (uint64_t i = 0; i < countm1 + 1; i++) {
        strings[i].header = malloc(sizeof(struct frame_header));
        strings[i].header->is_string = true;
        strings[i].header->links.prev_offset = i == 0 ? frame_offset : offset - header->frame_size;
        strings[i].header->links.next_offset = i == countm1 ? 0 : offset + header->frame_size;

        strings[i].real_length = i == countm1 && len % header->frame_bytes != 0
                                     ? len % header->frame_bytes
                                     : header->frame_bytes;
        strings[i].data = malloc(sizeof(char) * strings[i].real_length);

        for (uint64_t j = 0; j < strings[i].real_length; j++) {
            // printf("%lu\n", i * header->frame_bytes + j);
            strings[i].data[j] = source[i * header->frame_bytes + j];
        }

        // printf("%lu %lu %lu %s\n", i, strings[i].header->links.prev_offset,
        //        strings[i].header->links.next_offset, strings[i].data);
        frame_seek(file, header, offset);
        string_write(file, header, &strings[i]);
        header->frame_count += 1;

        offset += header->frame_size;
    }
    header_save(file, header);
}

int string_load(FILE *file, struct database_header *header, char **target, uint64_t offset) {
    uint64_t length = 0;
    uint64_t frames = 0;

    struct string *temp_string = malloc(sizeof(struct string));
    string_init(header, temp_string);
    temp_string->header->links.next_offset = offset;
    temp_string->real_length = header->frame_bytes;

    while (temp_string->real_length == header->frame_bytes &&
           temp_string->header->links.next_offset != 0) {
        frame_seek(file, header, temp_string->header->links.next_offset);
        string_read(file, header, temp_string);
        // printf("%lu %lu %lu %s\n", temp_string->real_length,
        // temp_string->header->links.prev_offset,
        //        temp_string->header->links.next_offset, temp_string->data);
        length += temp_string->real_length;
        frames++;
    }

    // printf("length: %lu\n", length);
    *target = malloc(sizeof(char) * length + 1);

    temp_string->header->links.next_offset = offset;
    for (uint64_t i = 0; i < frames; i++) {
        frame_seek(file, header, temp_string->header->links.next_offset);
        string_read(file, header, temp_string);
        for (uint64_t j = 0; j < temp_string->real_length; j++) {
            (*target)[i * header->frame_bytes + j] = temp_string->data[j];
        }
    }
    (*target)[length] = 0;

    string_free(temp_string);
}
