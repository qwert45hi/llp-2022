#include "meta.h"

int header_write(FILE *file, struct database_header *header) {
    fwrite(header, sizeof(struct database_header), 1, file);
    return 0;
}

int header_save(FILE *file, struct database_header *header) {
    fseek(file, 0, SEEK_SET);
    header_write(file, header);
    return 0;
}

int header_read(FILE *file, struct database_header *header) {
    fread(header, sizeof(struct database_header), 1, file);
    return 0;
}

int header_load(FILE *file, struct database_header *header) {
    fseek(file, 0, SEEK_SET);
    header_read(file, header);
    return 0;
}

uint64_t database_frames_end_offset(struct database_header *header) {
    return header->frame_offset + header->frame_size * header->frame_count;
}

int database_seek_frames_end(FILE *file, struct database_header *header) {
    fseek(file, database_frames_end_offset(header), SEEK_SET);
}
