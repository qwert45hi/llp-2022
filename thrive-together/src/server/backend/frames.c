#include "frames.h"

int frame_write(FILE *file, struct database_header *header, struct frame *frame) {
    fwrite(frame->header, sizeof(struct frame_header), 1, file);
    fwrite(frame->data, sizeof(union data_type), header->field_count, file);
    return 0;
}

void frame_init(struct database_header *header, struct frame *frame) {
    frame->header = malloc(sizeof(struct frame_header));
    frame->data = malloc(sizeof(union data_type) * header->field_count);
}

void frame_free(struct frame *frame) {
    free(frame->header);
    free(frame->data);
}

int frame_header_read(FILE *file, struct database_header *header,
                      struct frame_header *frame_header) {
    fread(frame_header, sizeof(struct frame_header), 1, file);
    return 0;
}

int frame_data_read(FILE *file, struct database_header *header, struct frame *frame) {
    fread(frame->data, sizeof(union data_type), header->field_count, file);
    return 0;
}

int frame_read(FILE *file, struct database_header *header, struct frame *frame) {
    frame_header_read(file, header, frame->header);
    frame_data_read(file, header, frame);
    return 0;
}

int frame_seek(FILE *file, struct database_header *header, uint64_t offset) {
    // printf("seek: %lu\n", offset);
    fseek(file, offset, SEEK_SET);
}

int frame_load(FILE *file, struct database_header *header, struct frame *frame, uint64_t offset) {
    frame_init(header, frame);
    frame_seek(file, header, offset);
    frame_read(file, header, frame);
}

int frame_append(FILE *file, struct database_header *header, struct frame *frame,
                 uint64_t *offset) {
    *offset = database_frames_end_offset(header);
    frame_seek(file, header, *offset);
    frame_write(file, header, frame);
    header->frame_count++;
    header_save(file, header);
    return 0;
}
