#include "utils.h"

int move_frame(FILE *file, struct database_header *header, uint64_t source, uint64_t target) {
    frame_seek(file, header, source);
    struct frame_header *frame_header = malloc(sizeof(struct frame_header));
    frame_header_read(file, header, frame_header);

    if (frame_header->is_string) {
        struct string *temp_string = malloc(sizeof(struct string));
        string_init(header, temp_string);

        temp_string->header->is_string = true;
        temp_string->header->links = frame_header->links;
        string_data_read(file, header, temp_string);
        // printf("%lu %s\n", frame_header->links.prev_offset, temp_string->data);

        write_zeroes(file, source, header->frame_size);
        // printf("cleared: %lu %lu\n", source, header->frame_size);

        frame_seek(file, header, target);
        string_write(file, header, temp_string);

        if (frame_header->links.next_offset != 0) {
            frame_seek(file, header, frame_header->links.next_offset);
            string_read(file, header, temp_string);
            temp_string->header->links.prev_offset = target;

            frame_seek(file, header, frame_header->links.next_offset);
            string_write(file, header, temp_string);

            // printf("hey %lu %lu\n", temp_string->header->links.prev_offset, target);
        }
        if (frame_header->links.prev_offset != 0) {  // TODO links to parent tuple
            frame_seek(file, header, frame_header->links.prev_offset);
            string_read(file, header, temp_string);
            temp_string->header->links.next_offset = target;

            frame_seek(file, header, frame_header->links.prev_offset);
            string_write(file, header, temp_string);
        }

        string_free(temp_string);
    } else {  // TODO may link to strings, whose prev_offset's have to be moved
        struct frame *temp_frame = malloc(sizeof(struct frame));
        frame_init(header, temp_frame);

        temp_frame->header->is_string = false;
        temp_frame->header->links = frame_header->links;
        frame_data_read(file, header, temp_frame);

        write_zeroes(file, source, header->frame_size);
        // printf("cleared: %lu %lu\n", source, header->frame_size);

        frame_seek(file, header, target);
        frame_write(file, header, temp_frame);

        frame_free(temp_frame);

        struct node *node = malloc(sizeof(struct node));
        node_load(file, header, node, frame_header->links.node_id);
        node->frame_offset = target;
        node_save(file, header, node, frame_header->links.node_id);
        free(node);
    }
    return 0;
}

int move_frame_to_end(FILE *file, struct database_header *header, uint64_t source) {
    uint64_t target = database_frames_end_offset(header);
    return move_frame(file, header, source, target);
}

int move_frame_from_end(FILE *file, struct database_header *header, uint64_t target) {
    uint64_t source = database_frames_end_offset(header) - header->frame_size;
    return move_frame(file, header, source, target);
}

int write_zeroes(FILE *file, uint64_t offset, uint64_t count) {
    fseek(file, offset, SEEK_SET);
    for (; count > 0; count--) fputc(0, file);
    return 0;
}
