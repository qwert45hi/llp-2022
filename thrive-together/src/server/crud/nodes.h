#ifndef NQ_1_NODES_H
#define NQ_1_NODES_H

#include <inttypes.h>
#include <stdio.h>

#include "../structures.h"
#include "utils.h"

uint64_t node_offset(struct database_header *header, uint64_t node_id);
int node_seek(FILE *file, struct database_header *header, uint64_t node_id);

int node_save(FILE *file, struct database_header *header, struct node *node, uint64_t node_id);
int node_append(FILE *file, struct database_header *header, struct node *node);

int node_read(FILE *file, struct database_header *header, struct node *node);
int node_load(FILE *file, struct database_header *header, struct node *node, uint64_t node_id);

#endif
