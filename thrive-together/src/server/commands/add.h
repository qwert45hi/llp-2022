#ifndef NQ_1_COMMANDS_ADD_H
#define NQ_1_COMMANDS_ADD_H

#include <malloc.h>
#include <stdio.h>
#include <stdlib.h>

#include "../backend/frames.h"
#include "../backend/strings.h"
#include "../crud/nodes.h"
#include "../structures.h"

int add_element(FILE *file, struct database_header *header, int argc, char **argv);

#endif
