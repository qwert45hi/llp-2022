#ifndef NQ_1_STRUCTURES_H
#define NQ_1_STRUCTURES_H

#include <inttypes.h>
#include <stdbool.h>

struct database_header {
    uint64_t field_count;   // the amount of defined fields
    uint64_t node_count;    // current amount of nodes in the file
    uint64_t node_offset;   // position in the file where nodes start
    uint64_t frame_size;    // data frame size in bytes
    uint64_t frame_bytes;   // data frame size in bytes
    uint64_t frame_count;   // current amount of data frames in the file
    uint64_t frame_offset;  // position in the file where data frames start
};

enum field_type { TYPE_INTEGER, TYPE_FLOAT, TYPE_BOOLEAN, TYPE_STRING };

struct field {
    uint64_t name_offset;  // offset to a data frame with the name
    enum field_type type;  // field type
};

struct node {
    int64_t parent_id;      // parent node id or -1
    uint64_t frame_offset;  // data frame offset
};

struct database {
    struct database_header* header;
    struct field* fields;
    struct node* nodes;
};

struct frame_header {
    bool is_string;
    union {
        uint64_t node_id;
        struct {
            uint64_t prev_offset;
            uint64_t next_offset;
        };
    } links;
};

union data_type {
    uint64_t string_offset;
    int64_t integer_data;
    double float_data;
    bool boolean_data;
};

struct frame {
    struct frame_header* header;
    union data_type* data;
};

struct string {
    struct frame_header* header;
    uint64_t real_length;
    char* data;
};

#endif
