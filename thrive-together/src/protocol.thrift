enum command_name {
  INSERT,
  FIND,
  DELETE,
  UPDATE,
}

enum operation_type {
  OP_EQ,
  OP_NE,
  OP_GT,
  OP_GE,
  OP_LT,
  OP_LE,
}

union field_value {
  1: i64 integer_value;
  2: double double_value;
  3: bool boolean_value;
  4: string string_value;
}

struct filter {
  1: string field,
  2: operation_type comparator,
  3: field_value value,
}

struct setter {
  1: string field,
  2: field_value value,
}

struct command {
  1: command_name name,
  2: optional list<filter> parent_filters,
  3: optional list<filter> filters,
  4: optional list<setter> setters,
}

enum status_code {
  CODE_200_SUCCESS,
  CODE_400_BAD_COMMAND,
  CODE_500_INTERNAL_ERROR,
}

struct result {
  1: status_code status_code,
  2: optional map<string, field_value> data,
  3: optional string error_message,
}

service nq_service {
  result mongo(1: command command);
}
