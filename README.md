# Навигация
### Лаб 1
- [Отчёт](https://gitlab.se.ifmo.ru/qwert45hi/llp-2022/-/tree/master/still-better-than-xml/report.pdf)
- [still-better-than-xml](https://gitlab.se.ifmo.ru/qwert45hi/llp-2022/-/tree/master/still-better-than-xml)

### Лаб 2
- [Отчёт](https://gitlab.se.ifmo.ru/qwert45hi/llp-2022/-/tree/master/yacc-mongo/report.pdf)
- [yacc-mongo](https://gitlab.se.ifmo.ru/qwert45hi/llp-2022/-/tree/master/yacc-mongo)

### Лаб 3
- [Отчёт](https://gitlab.se.ifmo.ru/qwert45hi/llp-2022/-/tree/master/thrive-together/report.pdf)
- [thrive-together](https://gitlab.se.ifmo.ru/qwert45hi/llp-2022/-/tree/master/thrive-together)

### Практическое задание
- Основной артефакт: [отчёт](https://gitlab.se.ifmo.ru/qwert45hi/llp-2022/-/tree/master/filesystem-in-a-file/report.pdf)
- [Папка](https://gitlab.se.ifmo.ru/qwert45hi/llp-2022/-/tree/master/filesystem-in-a-file/) с дополнительными инструментами
