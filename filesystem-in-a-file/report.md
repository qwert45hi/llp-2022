## Задание
Импортировать в разработанную программу информацию о сущностях и отношениях между ними

Из папок /lib* достать файловое дерево и записать каждый файл как элемент, а отношения восстановить по связям папка-содержимое. Для каждого файла достать: название, размер, дату создания, дату изменения, имя владельца, имя группы-владельца, а для текстовых также добавить содержимое

Дописать два разных запроса к полученному датасету:
1. Два условия на поля одной сущности
2. Два условия на поля разных сущностей из отношений

## Решение
### Обход дерева
Для обхода используется python-скрипт `generator.py`, у которого есть две команды:
1. traverse: обходит данную папку рекурсивно и создаёт для каждого файла запись с нужными данными
2. parse: превращает вывод первой команды в команды для основной программы

Для удобства и платформо-независимости, скрипт запускается в docker-контейнере (для доступа к папке `/lib` в докере на windows нужно добавить ядро linux в WSL или использовать другую папку). Полученные команды запускаются на пустом файле, генерируя базу данных для дальнейших запросов

### Простой запрос
#### Запрос
```mongo
db.practice.find({created: {$le: 1550000000}, group: {$eq: "root"}})
```

#### Дерево разбора запроса
```yaml
file: "practice"
command: "find"
filters:
  - field: "created"
    comparator: 5  # less or equal
    value: 1550000000
  - field: "group"
    comparator: 0  # equal
    value: "root"
```

#### Результат выборки
Текст в content обрезан ради читабельности
```json
[
  {
    "entry_id": 451,
    "parent": 450,
    "name": "python3.supp",
    "size": 8935,
    "created": 1679754611.7881992,
    "updated": 1528234536.0,
    "user": "root",
    "group": "root",
    "content": "#\n# This is a valgrind suppression file that should be used when using valgrind"
  },
  {
    "entry_id": 467,
    "parent": 466,
    "name": "gpg-agent.service",
    "size": 223,
    "created": 1679754611.1001992,
    "updated": 1503915774.0,
    "user": "root",
    "group": "root",
    "content": "[Unit]\nDescription=GnuPG cryptographic agent and passphrase cache\n"
  },
  {
    "entry_id": 468,
    "parent": 466,
    "name": "gpg-agent.socket",
    "size": 234,
    "created": 1679754611.1001992,
    "updated": 1503915774.0,
    "user": "root",
    "group": "root",
    "content": "[Unit]\nDescription=GnuPG cryptographic agent and passphrase cache\n"
  },
  {
    "entry_id": 469,
    "parent": 466,
    "name": "dirmngr.service",
    "size": 212,
    "created": 1679754611.1001992,
    "updated": 1503915774.0,
    "user": "root",
    "group": "root",
    "content": "[Unit]\nDescription=GnuPG network certificate management daemon\n"
  },
  {
    "entry_id": 470,
    "parent": 466,
    "name": "gpg-agent-extra.socket",
    "size": 281,
    "created": 1679754611.1001992,
    "updated": 1503915774.0,
    "user": "root",
    "group": "root",
    "content": "[Unit]\nDescription=GnuPG cryptographic agent and passphrase cache (restricted)\n"
  },
  {
    "entry_id": 471,
    "parent": 466,
    "name": "gpg-agent-ssh.socket",
    "size": 308,
    "created": 1679754611.1001992,
    "updated": 1503915774.0,
    "user": "root",
    "group": "root",
    "content": "[Unit]\nDescription=GnuPG cryptographic agent (ssh-agent emulation)\n"
  },
  {
    "entry_id": 472,
    "parent": 466,
    "name": "dirmngr.socket",
    "size": 204,
    "created": 1679754611.1001992,
    "updated": 1503915774.0,
    "user": "root",
    "group": "root",
    "content": "[Unit]\nDescription=GnuPG network certificate management daemon\n"
  },
  {
    "entry_id": 473,
    "parent": 466,
    "name": "gpg-agent-browser.socket",
    "size": 298,
    "created": 1679754611.1001992,
    "updated": 1503915774.0,
    "user": "root",
    "group": "root",
    "content": "[Unit]\nDescription=GnuPG cryptographic agent and passphrase cache (access for web browsers)\n"
  }
]
```

#### Эталонный результат
Эталонный результат получен через обработку полученного из команды `traverse` json-а следующим скриптом (python):
```py
from json import load, dumps

with open("./filesystem-in-a-file/traversed.json", encoding="utf-8") as f:
    data: list[dict[str, ...]] = load(f)

result: list[dict[str, ...]] = [d for d in data if d["updated"] <= 1550000000 and d["group"] == "root"]
print(dumps(result, ensure_ascii=False, indent=2))
```

Он совпадает с полученным программой с точностью до порядка ключей в объектах и конкретных id

## Запрос с отношением
#### Запрос
```mongo
db.practice.find({parent: {name: {$eq: "__pycache__"}}, size: {$gt: 50000}})
```

#### Дерево разбора запроса
```yaml
file: "practice"
command: "find"
parent:
  - field: "name"
    comparator: 0  # equal
    value: "__pycache__"
FILTERS:
  - field: "size"
    comparator: 2  # greater
    value: 50000
```

#### Результат выборки
```json
[
  {
    "entry_id": 319,
    "parent": 1981,
    "name": "doctest.cpython-39.pyc",
    "size": 76004,
    "created": 1679754611.6321993,
    "updated": 1679551306.0,
    "user": "root",
    "group": "root",
    "content": ""
  },
  {
    "entry_id": 321,
    "parent": 1981,
    "name": "mailbox.cpython-39.pyc",
    "size": 60598,
    "created": 1679754611.6361992,
    "updated": 1679551306.0,
    "user": "root",
    "group": "root",
    "content": ""
  },
  {
    "entry_id": 337,
    "parent": 314,
    "name": "typing.cpython-39.pyc",
    "size": 71311,
    "created": 1679754611.6481993,
    "updated": 1679551303.0,
    "user": "root",
    "group": "root",
    "content": ""
  },
  {
    "entry_id": 342,
    "parent": 1981,
    "name": "_pydecimal.cpython-39.pyc",
    "size": 160611,
    "created": 1679754611.6281993,
    "updated": 1679551306.0,
    "user": "root",
    "group": "root",
    "content": ""
  },
  {
    "entry_id": 361,
    "parent": 5543,
    "name": "argparse.cpython-39.pyc",
    "size": 63706,
    "created": 1679754611.6281993,
    "updated": 1679551302.0,
    "user": "root",
    "group": "root",
    "content": ""
  },
  {
    "entry_id": 362,
    "parent": 3214,
    "name": "pickletools.cpython-39.pyc",
    "size": 67105,
    "created": 1679754611.6401992,
    "updated": 1679551306.0,
    "user": "root",
    "group": "root",
    "content": ""
  }
]
```

#### Эталонный результат
Эталонный результат получен через обработку полученного из команды `traverse` json-а следующим скриптом (python):
```py
from json import load, dumps

with open("./filesystem-in-a-file/traversed.json", encoding="utf-8") as f:
    data: list[dict[str, ...]] = load(f)
parent_ids: str[int] = {d["entry_id"] for d in data if d["name"] == "__pycache__"}
result: list[dict[str, ...]] = [d for d in data if d["parent"] in parent_ids and d["updated"] > 1679551305.0]
print(dumps(result, ensure_ascii=False, indent=2))
```

Он совпадает с полученным программой с точностью до порядка ключей в объектах и конкретных id

## Выводы
1. Разработанная программа хорошо показала себя в этом практическом задании
2. Демонстрация работы программы на реальных примерах помогает выявить баги системы
3. Инструментарий pathlib в python может больше, чем нужно
4. Различия операционных систем могут всплыть в самых неожиданных местах

### PS
Остальные работы на подходе, будут заливаться в репозиторий по мере готовности
