from collections.abc import Iterator
from grp import getgrgid
from json import dump, load
from pathlib import Path
from pwd import getpwuid

from pydantic import BaseModel, parse_obj_as
from typer import Argument, FileText, FileTextWrite, Typer

app = Typer()
text_chars = bytearray({7, 8, 9, 10, 12, 13, 27} | set(range(0x20, 0x100)) - {0x7F})


class Counter:
    def __init__(self):
        self.value: int = -1

    def __next__(self):
        self.value += 1
        return self.value


class FileSystemEntry(BaseModel):
    entry_id: int
    parent: int
    name: str
    size: int
    created: float
    updated: float
    user: str
    group: str
    content: str

    def to_command(self) -> str:
        return (
            f"{self.parent} "
            f'"{self.name}" {self.size} '
            f"{self.created} {self.updated} "
            f'"{self.user}" "{self.group}" '
            f'"{self.content}"'
        )


def traverse_directory(
    directory: Path,
    counter: Counter,
    parent: int = -1,
    level: int = 0,
) -> Iterator[FileSystemEntry]:
    for i, path in enumerate(directory.iterdir()):
        is_text: bool = False
        if path.is_file():
            with path.open("rb") as f:
                is_text = not f.read(1024).translate(None, text_chars)

        content: str = ""
        if is_text:
            with path.open(encoding="utf-8") as f:
                content = f.read()

        entry_id = next(counter)
        stat = path.stat()
        yield FileSystemEntry(
            entry_id=entry_id,
            name=path.name,
            size=stat.st_size,
            created=stat.st_ctime,
            updated=stat.st_mtime,
            user=getpwuid(stat.st_uid)[0],
            group=getgrgid(stat.st_gid)[0],
            content=content,
            parent=parent,
        )

        if path.is_dir():
            yield from traverse_directory(path, counter, entry_id, level + 1)


@app.command("traverse")
def traverse_tree(output_file: FileTextWrite = Argument(...), root_dir: Path = "/lib"):
    assert root_dir.is_dir()
    result = list(traverse_directory(root_dir, Counter()))
    dump([entry.dict() for entry in result], output_file)


@app.command("parse")
def parse_tree(
    source_file: FileText = Argument(...),
    output_file: FileTextWrite = Argument(...),
):
    source = parse_obj_as(list[FileSystemEntry], load(source_file))
    for element in source:
        output_file.write(f"./main add {element.to_command()}\n")


if __name__ == "__main__":
    app()
